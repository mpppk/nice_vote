module.exports = {
  entry: {
    app: './src/main.jsx',
    result: './src/result.jsx'
  },
  output: {
    filename: './build/[name].bundle.js'
  },
  devtool: 'inline-source-map',
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel', // 'babel-loader' is also a legal name to reference
        query: { presets: ['es2015', 'react'] }
      }
    ]
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  }
};
