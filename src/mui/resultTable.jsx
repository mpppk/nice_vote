import Table from 'material-ui/lib/table/table';
import TableRow from 'material-ui/lib/table/table-row';
import TableHeader from 'material-ui/lib/table/table-header';
import TableRowColumn from 'material-ui/lib/table/table-row-column';
import TableHeaderColumn from 'material-ui/lib/table/table-header-column';
import TableBody from 'material-ui/lib/table/table-body';

var React = require('react');
const VoteResultTable = React.createClass({

  getTextColor(colorName){
    switch (colorName) {
    case 'blue':
      return 'royalblue';
    case 'white':
      return 'black';
    case 'red':
      return 'firebrick';
    case 'green':
      return 'green';
    case 'yellow':
      return 'goldenrod';

    default:
      return colorName;

    }
  },

  createTableHeader(teamName, cols){
    // let createHeaderRowCols = (col) =>{
    //   return <TableHeaderColumn key={col}>{col}</TableHeaderColumn>;
    // };
    let color = this.getTextColor(teamName);
    return(
      <TableHeader>
        <TableRow>
          <TableHeaderColumn colSpan={cols.length} style={{
            textAlign: 'center', fontSize: '40px', color: color}}>
            {teamName}
          </TableHeaderColumn>
        </TableRow>
      </TableHeader>
    );
    // <TableRow>
    //   {cols.map(createHeaderRowCols)}
    // </TableRow>
  },

  createTableRow(cols){

    return(
      <TableRow>
        {cols.map(this.createTableCols)}
      </TableRow>
    );
  },

  createTableCols(col){
    return(
      <TableRowColumn>{col}</TableRowColumn>
    );
  },

  render: function(){
    let rows = this.props.rows;
    return (
      <Table style={{border:'solid'}}>
        {this.createTableHeader(this.props.title, this.props.headers)}
        <TableBody>
          {rows.map(this.createTableRow)}
        </TableBody>
      </Table>
    );
  }
});

module.exports = VoteResultTable;
