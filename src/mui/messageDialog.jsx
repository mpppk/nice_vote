let injectTapEventPlugin = require('react-tap-event-plugin');
injectTapEventPlugin();

import ReactFireMixin from 'reactfire';
import Firebase from 'firebase';

import React from 'react';
import Dialog from 'material-ui/lib/dialog';

const FireBaseURL = 'https://sysrdc-yokyo.firebaseio.com/';

const MessageCard = React.createClass({
  mixins: [ReactFireMixin],

  answersRef: new Firebase(FireBaseURL + 'message'),

  standardActions: [
    { text: 'OK' }
  ],

  getInitialState: function() {
    return {
      messageDialog: {}
    };
  },

  componentWillMount: function() {
    var url = FireBaseURL + '/users/' + this.props.userId + '/messageDialog';
    console.log('message dialog url: ' + url);
    var firebaseRef = new Firebase(url);
    this.bindAsObject(firebaseRef, 'messageDialog', function(error){
      console.log('error! ' + error);
    });

    // ダイアログを表示するタイミング
    firebaseRef.on('value', (snapshot) => {
      console.log('message dialog value called');
      if(snapshot.val() !== null){
        this.refs.messageDialog.show();
      }
    });
  },

  render(){
    return (
      <Dialog
        ref='messageDialog'
        title={this.state.messageDialog.title}
        actions={this.standardActions}
        open={true} >
        {this.state.messageDialog.content}
      </Dialog>
    );
  }
});

module.exports = MessageCard;
