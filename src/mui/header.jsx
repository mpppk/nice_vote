let injectTapEventPlugin = require('react-tap-event-plugin');
injectTapEventPlugin();

const React = require('react');
const AppBar = require('material-ui/lib/app-bar');

import LeftNav from 'material-ui/lib/left-nav';
import MenuItem from 'material-ui/lib/menu/menu-item';
import FlatButton from 'material-ui/lib/flat-button';
import Menu from './menu';

const Header = React.createClass({
  // getInitialState(){
  //   return {userId: cookie.load('userId')};
  // },

  onLeftIconTap(){
    this.refs.menu.toggle();
  },

  render() {
    const rightMessage = 'your team is ' + this.props.teamName;
    return (
      <AppBar
        title="Nice Vote"
        iconElementRight={<FlatButton label={rightMessage} />}
        onLeftIconButtonTouchTap={this.onLeftIconTap} >
        <Menu/>
        <LeftNav ref="menu" docked={false}>
          <MenuItem index={0}>What it is</MenuItem>
          <MenuItem index={1}>
            <a href="/link">Your ID is {this.props.userId}</a>
          </MenuItem>
        </LeftNav>
      </AppBar>
    );
  }
});

module.exports = Header;
