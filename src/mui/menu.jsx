let injectTapEventPlugin = require('react-tap-event-plugin');
injectTapEventPlugin();

import React from 'react';
import LeftNav from 'material-ui/lib/left-nav';
// import MenuItem from 'material-ui/lib/menu/index';
import MenuItem from 'material-ui/lib/menu/menu-item';

export default class Menu extends React.Component{
  render(){
    return (
      <LeftNav ref="menu" docked={false}>
        <MenuItem index={0}>Menu Item</MenuItem>
        <MenuItem index={1}><a href="/link">Link</a></MenuItem>
      </LeftNav>
    );
  }
}
