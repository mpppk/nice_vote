let injectTapEventPlugin = require('react-tap-event-plugin');
injectTapEventPlugin();

import React from 'react';

import Dialog from 'material-ui/lib/dialog';

export default class QuestionTab extends React.Component{
  constructor(props){
    super(props);
    this.standardActions = [
      { text: 'OK' }
    ];
  }

  componentWillUpdate(nextProps){
    if(nextProps.open){
      this.refs.timeLimitDialog.show();
    }
  }

  render(){
    return (
      <Dialog
        ref='timeLimitDialog'
        title="回答受付は終了しました。"
        actions={this.standardActions}
        open={true} >
        正解発表をお待ちください。
      </Dialog>
    );
  }
}
