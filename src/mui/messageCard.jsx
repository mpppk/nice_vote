let injectTapEventPlugin = require('react-tap-event-plugin');
injectTapEventPlugin();

import ReactFireMixin from 'reactfire';
import Firebase from 'firebase';

import React from 'react';
import Card from 'material-ui/lib/card/card';
import CardHeader from 'material-ui/lib/card/card-header';
import CardText from 'material-ui/lib/card/card-text';
import Avatar from 'material-ui/lib/avatar';

const FireBaseURL = 'https://sysrdc-yokyo.firebaseio.com/';

const MessageCard = React.createClass({
  mixins: [ReactFireMixin],

  answersRef: new Firebase(FireBaseURL + 'message'),

  getInitialState: function() {
    return {
      message: {}
    };
  },

  componentWillMount: function() {
    var firebaseRef = new Firebase(FireBaseURL + '/message');
    this.bindAsObject(firebaseRef, 'message', function(error){
      console.log('error! ' + error);
    });
  },

  render(){
    return (
      <Card>
        <CardHeader
          title={this.state.message.title}
          avatar={<Avatar>M</Avatar>}
          actAsExpander={true}
          showExpandableButton={true}>
        </CardHeader>
        <CardText>
          {this.state.message.content}
        </CardText>
      </Card>
    );
  }
});

module.exports = MessageCard;
