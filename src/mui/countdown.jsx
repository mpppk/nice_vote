let injectTapEventPlugin = require('react-tap-event-plugin');
injectTapEventPlugin();

import React from 'react';
import LinearProgress from 'material-ui/lib/linear-progress';
import TimeLimitDialog from './timeLimitDialog';

const style = {
  margin: '20px 0px'
};

export default class Countdown extends React.Component{
  constructor(props){
    super(props);
    const startTime = new Date();
    this.state = {
      remainTime: this.getSecDiff(),
      startTime: startTime,
      open: false
    };
  }

  getSecDiff(){
    const currentTime = new Date();
    const msDiff = this.props.limitDate.getTime() - currentTime.getTime();
    return msDiff / 1000;
  }

  getSecDiffFromStart(){
    const msDiff =
      this.props.limitDate.getTime() - this.state.startTime.getTime();
    return msDiff / 1000;
  }

  onUpdateCountdown(){
    // １度だけtimeupイベントを発行するために利用
    let beforeRemainTime = this.state.remainTime;
    let remainTime = (this.getSecDiff() > 0) ? this.getSecDiff() : 0;
    // 時間切れになったタイミングを検出
    if(beforeRemainTime != 0 && remainTime == 0){
      this.onTimeUp();
    }

    // カウントダウンが開始したタイミングを検出
    if(beforeRemainTime == 0 && remainTime != 0){
      this.onStartCountdown();
    }

    let dialogDidOpenFlag = false;
    if(this.state.dialogWillOpenFlag == true){
      dialogDidOpenFlag = true;
      console.log('state open flag: ' + this.state.dialogWillOpenFlag);
    }

    this.setState({
      dialogDidOpenFlag: dialogDidOpenFlag,
      dialogWillOpenFlag: false,
      beforeRemainTime: beforeRemainTime,
      remainTime: remainTime,
      startTime: this.state.startTime
    });
  }

  // 時間切れになった際に呼ばれる
  onTimeUp(){
    console.log('time up!!!!');
    this.setState({dialogWillOpenFlag: true});
    this.props.onTimeUp();
  }

  // カウントダウンが開始された際に呼ばれる
  onStartCountdown(){
    this.props.onStartCountdown();
  }

  getProgress(){
    if(this.state.remainTime < 0 && this.getSecDiffFromStart() < 0){
      return 0;
    }

    const progressPer =
      this.state.remainTime / this.getSecDiffFromStart() * 100;
    console.log('progress: ' + progressPer);
    return progressPer;
  }

  componentDidMount(){
    this.timerId = setInterval(this.onUpdateCountdown.bind(this), 1000);
  }

  componentWillUnmount(){
    clearInterval(this.timerId);
  }

  render(){
    let dialogOpenFlag = this.state.dialogWillOpenFlag;
    const progress = this.getProgress();
    return (
      <div>
        {this.state.remainTime}
        <LinearProgress mode="determinate"
          value={progress}
          style={style} />
        <TimeLimitDialog open={dialogOpenFlag}/>
      </div>

    );
  }
}
