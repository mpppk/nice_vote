let injectTapEventPlugin = require('react-tap-event-plugin');
injectTapEventPlugin();

import ReactFireMixin from 'reactfire';
import Firebase from 'firebase';

import React from 'react';
import Tabs from 'material-ui/lib/tabs/tabs';
import Tab from 'material-ui/lib/tabs/tab';
import Card from 'material-ui/lib/card/card';
import CardHeader from 'material-ui/lib/card/card-header';
import Avatar from 'material-ui/lib/avatar';

import Choices from './choices';

const FireBaseURL = 'https://sysrdc-yokyo.firebaseio.com/items/';

const QuestionTabs = React.createClass({
  mixins: [ReactFireMixin],

  answersRef: new Firebase(FireBaseURL + 'answers'),

  getInitialState: function() {
    return {
      items: [],
      text: ''
    };
  },

  componentWillMount: function() {
    var firebaseRef = new Firebase(FireBaseURL);
    this.bindAsArray(firebaseRef.limitToLast(25), 'items');
  },

  // コンポーネントとして分離
  createQuestionTab: function(item, index) {
    const questionNo = index + 1;
    return (
      <Tab label={'Q' + questionNo} key={index} >
        <Card>
          <CardHeader
            title={'Q' + questionNo + ' ' + item.text}
            avatar={<Avatar style={{color:'black'}}>Q</Avatar>}
            actAsExpander={true}
            showExpandableButton={true}>
            {}
          </CardHeader>
          <Choices choicesText={item.choices} questionIndex={questionNo}
            userId={this.props.userId}
            disabled={this.props.choicesDisabled}/>
        </Card>
      </Tab>
    );
  },

  render() {
    return (
      <Tabs>{this.state.items.map(this.createQuestionTab)}</Tabs>
    );
  }
});

module.exports = QuestionTabs;
