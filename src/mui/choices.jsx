let injectTapEventPlugin = require('react-tap-event-plugin');
injectTapEventPlugin();

import React from 'react';
import RadioButtonGroup from 'material-ui/lib/radio-button-group';
import RadioButton from 'material-ui/lib/radio-button';

import Firebase from 'firebase';
const FireBaseURL = 'https://sysrdc-yokyo.firebaseio.com/';

export default class Choices extends React.Component{
  constructor(props){
    super(props);

    var self = this;
    var ref = this.getUserAnswerRef(this.props.questionIndex,
      this.props.userId);
    if(typeof this.state === 'undefined'){
      ref.on('value', (snapshot) => {
        if(!snapshot.exists()){ return; }
        self.setState({answer: snapshot.val()});
      }, (errorObject)=>{
        console.log('The read failed: ' + errorObject.code);
      });
    }
  }

  getUserAnswerRef(questionIndex, userId){
    let url = FireBaseURL + 'answers/' + questionIndex + '/' + userId;
    return new Firebase(url);
  }

  onAnswerChanged(event, selected){
    let ref = this.getUserAnswerRef(this.props.questionIndex,
      this.props.userId);
    ref.set(selected);
  }

  createChoices(item, index){
    return (
      <RadioButton key={index}
        value={item}
        label={item}
        style={{marginBottom:16, marginLeft: 16}}
        disabled={this.props.disabled}
        />
    );
  }

  render(){
    let selected = 'くまモン';
    if(this.state && this.state.answer){
      selected = this.state.answer;
    }
    return (
      <RadioButtonGroup name="shipSpeed" valueSelected={selected}
        questionIndex={this.props.questionIndex}
        onChange={this.onAnswerChanged.bind(this)}
        userId={this.props.userId} >
        {this.props.choicesText.map(this.createChoices.bind(this))}
      </RadioButtonGroup>
    );
  }
}
