import ReactFireMixin from 'reactfire';
import Firebase from 'firebase';
import VoteResultTable from './mui/resultTable';

var React = require('react');
var ReactDOM = require('react-dom');
var Fireproof = require('fireproof');
var Q = require('q');
Fireproof.bless(Q);

// var Constraints = require('./constraints.js');

const FireBaseURL = 'https://sysrdc-yokyo.firebaseio.com/';
const Content = React.createClass({
  mixins: [ReactFireMixin],

  getInitialState: function() {
    return {
      answers: {},
      teams: {},
      questions: {}
    };
  },

  componentWillMount: function() {
    const answersRef = new Firebase(FireBaseURL + '/answers');
    this.bindAsObject(answersRef, 'answers', function(error){
      console.log('error! ' + error);
    });

    const teamsRef = new Firebase(FireBaseURL + '/teams');
    this.bindAsObject(teamsRef, 'teams', function(error){
      console.log('error! ' + error);
    });

    const questionsRef = new Firebase(FireBaseURL + '/items');
    this.bindAsArray(questionsRef, 'questions', function(error){
      console.log('error! ' + error);
    });
  },

  createQuestionInfos: function(){
    let result = {};
    let answers = this.state.answers;
    for (let key in answers) {
      result[key] = {};
      let answerOfQuestion = answers[key];
      for(let aKey in answers[key]){
        if(typeof result[key][answerOfQuestion[aKey]] === 'undefined'){
          result[key][answerOfQuestion[aKey]] = 0;
        }
        result[key][answerOfQuestion[aKey]]++;
      }
    }
    return result;
  },

  // チームごとに各設問の正解数を計算する
  createTeamInfos: function(){
    const answers = this.state.answers;
    let result;
    for(let questionIndex in answers){
      result = this.createTeamInfoOfQuestion(questionIndex, result);
    }
    return result;
  },

  createTeamInfoOfQuestion: function(questionNo, beforeResult){
    let result = beforeResult || {};
    const teams = this.state.teams;
    const answersOfQuestion = this.state.answers[questionNo];
    const question = this.state.questions[questionNo-1] || {};
    const choices = question.choices || [];
    for (let teamName in teams) {
      if(teamName === '.key'){continue;}

      result[teamName] = result[teamName] || {};
      result[teamName][questionNo] = result[teamName][questionNo] || {};

      result[teamName][questionNo]['choices'] = {};
      result[teamName][questionNo]['correct'] = question.correct;
      for(let i in choices){
        let choice = choices[i];
        result[teamName][questionNo]['choices'][choice] = 0;
      }

      // console.log(answersOfQuestion);
      for(let teamUserIndex in teams[teamName]){
        let userId = teams[teamName][teamUserIndex];
        let answer = answersOfQuestion[userId];
        if(typeof answer !== 'undefined'){
          result[teamName][questionNo]['choices'][answer]++;
        }
      }
    }
    return result;
  },

  createTeamInfoRows(teamInfo, onlyCorrectFlag){
    let result = {};
    for(let teamName in teamInfo){
      result[teamName] = [];
      let correctSum = 0;

      for(let questionNo in teamInfo[teamName]){
        for(let choice in teamInfo[teamName][questionNo]['choices']){
          let row = [];
          if(onlyCorrectFlag == true &&
            choice != teamInfo[teamName][questionNo]['correct']){
            continue;
          }

          row.push('Q' + questionNo);
          row.push(choice);
          row.push(teamInfo[teamName][questionNo]['choices'][choice]);
          result[teamName].push(row);
          correctSum += teamInfo[teamName][questionNo]['choices'][choice];
        }
      }
      let sumRow = ['ALL', 'CORRECT ANSWERS', correctSum];
      result[teamName].push(sumRow);
    }
    return result;
  },

  render: function(){
    const questionInfos = this.createQuestionInfos() || {};
    const teamInfos = this.createTeamInfos() || {};
    console.log('questionInfos');
    console.log(questionInfos);
    console.log('teamInfos');
    console.log(teamInfos);
    const teamInfoRows = this.createTeamInfoRows(teamInfos, true) || [];
    let tables = [];
    let headers = ['question', 'answer', 'count'];
    for(let teamName in teamInfoRows){
      let rows = teamInfoRows[teamName] || [];

      tables.push(
        <VoteResultTable title={teamName} headers={headers} rows={rows}
          key={teamName}/>
      );
    }
    return (
      <div>
        {tables}
      </div>
    );
  }
});

ReactDOM.render(<Content />, document.getElementById('content'));
