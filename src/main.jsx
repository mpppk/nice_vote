var React = require('react');
var ReactDOM = require('react-dom');
var Firebase = require('firebase');
var Fireproof = require('fireproof');
var Q = require('q');
Fireproof.bless(Q);

import cookie from 'react-cookie';

const Header = require('./mui/header');
import Countdown from './mui/countdown';
import QuestionTabs from './mui/questionTabs';
import MessageCard from './mui/messageCard';
import MessageDialog from './mui/messageDialog';

const FireBaseURL = 'https://sysrdc-yokyo.firebaseio.com/';
const Content = React.createClass({

  getInitialState(){
    let userId = cookie.load('userId');
    if(typeof userId === 'undefined'){
      const usersRef = new Firebase(FireBaseURL + '/users');
      userId = usersRef.push().key();
      cookie.save('userId', userId);

      // チーム分け
      let self = this;
      this.chooseTeam().then(function(teamName){
        console.log('your team is ' + teamName);
        self.addUserToTeam(userId, teamName);
      }, function(err){
        console.log('error: ' + err);
      });
    }else{
      // cookieにユーザIDが残っていた場合
      this.getOrCreateUserByUserId(userId).then((user) => {
        const teamName = user.team;
        // team名がFirebase上に存在しない場合
        if(teamName === ''){
          this.chooseTeam().then((teamName) => {
            console.log('your team is ' + teamName);
            this.addUserToTeam(userId, teamName);
          }, function(err){
            console.log('error: ' + err);
          });
        }

        this.setState({teamName: teamName});
      }, (error) => {
        console.log('error in getTeamNameByUserId: ' + error);
      });
    }
    console.log('ID is ' + userId);
    return {
      userId: userId,
      timeLimit: this.getAfterFewMinutesDate(5),
      teamName: ''
    };
  },

  getOrCreateUserByUserId(userId){
    const usersURL = FireBaseURL + '/users/';
    const firebaseRef = new Firebase(usersURL);
    const proofRef = new Fireproof(firebaseRef);
    return proofRef.child(userId).once('value').then(function(snapshot){
      if(snapshot.val() === null){
        const newUser = {};
        newUser[userId] = {team: ''};
        proofRef.update(newUser);
      }
      return Promise.resolve(snapshot.val() || newUser);
    });
  },

  chooseTeam(){
    const ref = new Firebase(FireBaseURL + '/teamMemberSum');
    const proofRef = new Fireproof(ref);
    const teamNames = ['red', 'blue', 'yellow', 'green'];

    return proofRef.once('value').then(function(snapshot){
      const data = snapshot.val();
      let min = 999999;
      let candidateTeamName = 'green';
      for(const teamName of teamNames){
        if(min > (data[teamName] || 0)){
          min = data[teamName];
          candidateTeamName = teamName;
        }
      }
      return Promise.resolve(candidateTeamName);
    });
  },

  // 指定したuserIDのデータが存在しなければ作る
  addUserToTeam(userId, teamName){
    console.log('add user to team');
    const ref = new Firebase(FireBaseURL);
    let user = {};
    user[userId] = {team: teamName};
    ref.child('users').update(user);
    // ref.child('users').child(userId).child('team').set(teamName);
    ref.child('teams').child(teamName).push(userId);
    const sumRef = ref.child('teamMemberSum').child(teamName);
    sumRef.transaction(function(currentValue){
      console.log('team member became ' + (currentValue + 1) );
      return (currentValue || 0) + 1;
    });

    // team名をstateに追加
    this.setState({teamName: teamName});
  },

  componentWillMount(){
    this.getTimeLimit();
  },

  getTimeLimit(){
    const ref = new Firebase(FireBaseURL + 'timeLimit');
    const self = this;
    ref.on('value', function(snapshot){
      console.log('timeLimit: ' + snapshot.val());
      self.setState({userId: self.state.userId,
        timeLimit: new Date(snapshot.val())});
    });
  },

  getAfterFewMinutesDate(minutes){
    const date = new Date();
    date.setMinutes(date.getMinutes() + minutes);
    return date;
  },

  // 時間切れになった際に呼ばれる
  onTimeUp(){
    console.log('time up in main');
    this.setState({choicesDisabled:true});
  },

  // 回答を受け付け始めた際に呼ばれる
  onStartCountdown(){
    console.log('start count down in main');
    this.setState({choicesDisabled:false});
  },

  render() {
    return (
      <div>
        <Header userId={this.state.userId} teamName={this.state.teamName}/>
        <Countdown limitDate={this.state.timeLimit}
          onTimeUp={this.onTimeUp}
          onStartCountdown={this.onStartCountdown}/>
        <QuestionTabs userId={this.state.userId}
          choicesDisabled={this.state.choicesDisabled}/>
        <MessageCard/>
        <MessageDialog userId={this.state.userId}/>
      </div>
    );
  }
});

ReactDOM.render(<Content />, document.getElementById('content'));
