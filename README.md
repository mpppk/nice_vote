# これは何
attack2500の回答ページです。  
回答は[Firebase](https://www.firebase.com/)というmBaaSにリアルタイムで保存されます。

# 動作イメージ
![こんな感じ](imgs/save_to_firebase_demo.gif)

左側がユーザの見る画面。  
左側はFirebaseに格納されたデータ。

# ページ一覧

[回答ページ](https://sysrdc-yokyo.firebaseapp.com/)

[格納されたFirebase上のデータ](https://sysrdc-yokyo.firebaseio.com/)

問題追加ページ(準備中)

結果確認ページ(準備中)

メッセージ送信ページ(準備中)
